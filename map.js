
// Function to mimic map function
function map(elements, cb){

    // Array that will be return after transformation
    let newArray = []

    for (let index = 0; index < elements.length; index++){

        let item = cb(elements[index], index);

        // Push new returned element into new array
        newArray.push(item);
    }

    return newArray;
}

// export The function
export {map};