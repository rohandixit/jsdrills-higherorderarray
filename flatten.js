// Function to flatten a given array

function flatten(elements){
   
    // If elements is an array than do this
    if (typeof(elements) == 'object') {

        // This will store the elements
        let ans = [];

        // Iterate through elements and merg them
        for (let element of elements) {
            ans = [...ans, ...flatten(element)];
        }
    
        // return merged array
        return ans;

    } else {
        return [elements];
    }
    
}

// Export the function
export {flatten};