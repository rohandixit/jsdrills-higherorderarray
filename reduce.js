// Function to mimic reduce function

function reduce(elements, cb, startingValue) {

    // Finaly will return
    let result;
    let indexStart = 0

    // Check if we have value there
    if (typeof(startingValue) == 'undefined'){

        result = elements[0];
        indexStart += 1

    } else {

        result = startingValue;

    }

    // Iterate through elements
    for (let index = indexStart; index < elements.length; index++){
    
        result = cb(result, elements[index]);

    }

    return result;
}

// Export the function
export {reduce};