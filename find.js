// Function to mimic find function

function find(elements, cb){

    for(let index = 0; index < elements.length; index++) {

        // If cb returns true for element than keep it
        if (cb(elements[index])) {
            
            return elements[index];
        }

    }
}

// Export the function
export {find};