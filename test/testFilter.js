import { filter } from "../filter.js";

// Test Array
let array = [1, 2, 3, 4, 5, 6];

// Test the Function for item greater than 3
let ans = filter(array, element => element > 3);

// print the result
console.log(ans)

// Test the function for item greater than 6
ans = filter(array, element => element > 6);

// print the result
console.log(ans)

// Test the function for item to be even
ans = filter(array, element => element%2 == 0);

// print the result
console.log(ans)