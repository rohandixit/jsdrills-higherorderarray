import { map } from "../map.js";

// Sample array to test on
let array = [1, 2, 3, 4, 5];

// Test The new function to get squrares
let newArray = map(array, element => element*element)

// Print the results
console.log(newArray);