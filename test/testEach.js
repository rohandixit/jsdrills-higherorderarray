import { each } from "../each.js";

// Sample array to test on
let array = [1, 2, 3, 4, 5];

// Second function that need to be passed in each function
function anyTestFunction(item, index){
    console.log(item, index);
}

// Test case for function each
each(array, anyTestFunction);