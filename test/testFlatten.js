import { flatten } from "../flatten.js";

// Testing array
const nestedArray = [1, [2], [[3]], [[[4]]]];

// Test the function.
let ans = flatten(nestedArray);

// Print the results
console.log(ans);