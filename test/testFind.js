import { find } from "../find.js";

// Test Array
let array = [1, 2, 3, 4, 5, 6];

// Test the Function for item greater than 3
let ans = find(array, element => element > 3);

// print the result
console.log(ans)

// Test the function for item greater than 6
ans = find(array, element => element > 6);

// print the result
console.log(ans)

// Test the function for item to be even
ans = find(array, element => element%2 == 0);

// print the result
console.log(ans)