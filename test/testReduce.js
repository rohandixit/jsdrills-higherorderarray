import { reduce } from "../reduce.js";

// Testing array
let array = [1, 2, 3, 4, 5, 6]

// Function to get sum
function sum(startingValue, item){
    return startingValue + item
}

// Function to get differnnce
function diff(startingValue, item){
    return startingValue - item;
}

// Test it here
let summation = reduce(array, sum);

let difference = reduce(array, diff, 100)

// Print The result

console.log("summation:", summation);
console.log("difference", difference)