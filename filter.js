// Function to mimic filter function

function filter(elements, cb){

    // To store elements that passed the criteria
    let newElements = [];

    for(let index = 0; index < elements.length; index++) {

        // If cb returns true for element than keep it
        if (cb(elements[index])) {
            newElements.push(elements[index]);
        }

    }

    return newElements;
}

// Export the function
export {filter};