// Function to mimic forEach function

function each(elements, cb) {

    // Iterate through elements
    for (let index = 0; index < elements.length; index++){

        // Now call the CB function
        cb(elements[index], index, elements)
    };
};

// Export The function
export {each};
